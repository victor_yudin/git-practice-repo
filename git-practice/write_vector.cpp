#include "stdafx.h"
#include "write_vector.h"


void FileWriter::writeVector(std::vector<int> data, std::string fileName)
{
	FILE *file;
	file = fopen(fileName.c_str(), "w");
	for (int item : data) {
		std::stringstream linee;
		linee << item;
		fputs(linee.str().c_str(), file);
		fputc('\n', file);
	}
	fclose(file);
}